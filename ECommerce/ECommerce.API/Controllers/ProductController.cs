﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Domain.DTO;
using ECommerce.Services.Interfaces;
using ECommerce.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ECommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet("All")]
        [ProducesResponseType(typeof(List<ProductItemResponse>), 200)]
        [Produces("application/json")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllProducts()
        {
            return Ok(await _productService.GetAll());
        }

        [HttpGet("{productId}")]
        [ProducesResponseType(typeof(ProductItemResponse), 200)]
        [Produces("application/json")]
        public async Task<IActionResult> GetProduct([Required] Guid productId)
        {
            return Ok(await _productService.GetDetail(productId));
        }

        [HttpPost]
        [ProducesResponseType(typeof(bool), 200)]
        [Produces("application/json", "multipart/form-data")]
        [Authorize(Roles = Constants.Roles.Admin)]
        public async Task<IActionResult> AddProduct([Required][FromForm] ProductItem productItem, List<IFormFile> images)
        {
            return Ok(await _productService.AddProduct(productItem,images));
        }

        [HttpPut]
        [ProducesResponseType(typeof(bool), 200)]
        [Produces("application/json", "multipart/form-data")]
        [Authorize(Roles = Constants.Roles.Admin)]
        public async Task<IActionResult> UpdateProduct([Required][FromForm] ProductItem productItem, List<IFormFile> images)
        {
            return Ok(await _productService.UpdateProduct(productItem,images));
        }

        [HttpDelete("{productId}")]
        [ProducesResponseType(typeof(bool), 200)]
        [Authorize(Roles = Constants.Roles.Admin)]
        [Produces("application/json")]
        public async Task<IActionResult> RemoveProduct([Required] Guid productId)
        {
            return Ok(await _productService.RemoveProduct(productId));
        }
    }
}
