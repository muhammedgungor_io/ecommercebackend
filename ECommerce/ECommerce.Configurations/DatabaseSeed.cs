﻿using ECommerce.Core.Context;
using ECommerce.Core.Interfaces;
using ECommerce.Domain.Entities;
using ECommerce.Utility.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Configurations
{
    /// <summary>
    /// Seeding database. 
    /// 
    /// There are some conditions for count control.Because we create a demo ecommerce platform therefore same entities must be unique and never has the same rows
    /// </summary>
    public class DatabaseSeed
    {
        public static async Task DoSeed(IServiceProvider serviceProvider)
        {
            await EnsureSeedData(
                serviceProvider.GetRequiredService<IConfiguration>(),
                serviceProvider.GetRequiredService<IUnitOfWork>(),
                 serviceProvider.GetRequiredService<ILogger<DatabaseSeed>>(),
                 serviceProvider.GetRequiredService<IECommerceCrypto>(),
                 serviceProvider.GetRequiredService<ECommerceDbContext>());
        }

        private static async Task EnsureSeedData(IConfiguration configuration, IUnitOfWork _uow, ILogger _logger, IECommerceCrypto _eCommerceCrypto, ECommerceDbContext dbContext)
        {
            _logger.LogInformation("Database Seeding Start.");

            try
            {
                dbContext.Database.Migrate();
                _logger.LogInformation("Database Migrations Done.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            #region Role Insert
            var roles = new List<Role> { new Role { AddDate = DateTime.Now, RoleName = "Admin" } };

            foreach (var role in roles)
            {
                await _uow.GetRepository<Role>().AddAsync(role);
            }
            #endregion

            #region User Insert
            var currentUsers = await _uow.GetRepository<User>().GetAllAsync();

            var temporaryPassword = "Password@123";

            var users = new List<User> { new User { Email = "mmgungor94@gmail.com", Name = "Muhammed" }, new User { Email = "admin@admin.com", Name = "Admin" } };

            if (currentUsers.Count() == 0)
            {
                foreach (var user in users)
                {
                    var encryptedPassword = _eCommerceCrypto.SetNewPassword(temporaryPassword);

                    user.Password = encryptedPassword.encryptedPassword;
                    user.SaltCode = encryptedPassword.saltCode;

                    await _uow.GetRepository<User>().AddAsync(user);
                }
            }

            #endregion

            #region UserRole

            var currentRoles = await _uow.GetRepository<UserRole>().GetAllAsync();
            if (currentRoles.Count() == 0)
            {
                var adminRoleId = roles.Find(c => c.RoleName.Equals("Admin")).Id;
                var adminUserId = users.Find(c => c.Name.Equals("Admin")).Id;

                var userRole = new UserRole { RoleId = adminRoleId, UserId = adminUserId };

                await _uow.GetRepository<UserRole>().AddAsync(userRole);
            }

            #endregion

            #region Product

            var currentProducts = await _uow.GetRepository<Product>().GetAllAsync();

            if (currentProducts.Count() == 0)
            {

                var products = new List<Product>() {
                     new Product { Amount = "5.000,00 €", Barcode = "barcodeString", Description = "Test product description 1", Name = "Test Product 1", Piece = 34 },
                     new Product { Amount = "7.000,00 €", Barcode = "barcodeString", Description = "Test product description 2", Name = "Test Product 2", Piece = 16 },
                     new Product { Amount = "11.000,00 €", Barcode = "barcodeString", Description = "Test product description 3", Name = "Test Product 3", Piece = 25 }
                };

                await _uow.GetRepository<Product>().AddRangeAsync(products);
            }

            #endregion

            #region Save Changes
            await _uow.SaveChangesAsync();
            #endregion

            _logger.LogInformation("Seeding Done !");
        }
    }
}
