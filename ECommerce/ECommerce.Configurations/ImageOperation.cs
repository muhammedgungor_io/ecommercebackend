﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECommerce.Configurations
{
    public class ImageOperation : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (context.MethodInfo.Name == "AddProduct")
            {
                operation.Parameters.Clear();

                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "Image",
                    In = ParameterLocation.Query,
                    Description = "Upload Product Image",
                    Required = true,
                    Content = new Dictionary<string, OpenApiMediaType>
                    {
                        {
                            "multipart/form-data",new OpenApiMediaType
                            {
                                Schema = new OpenApiSchema
                                {
                                    Type = "file",
                                    Format = "binary"
                                }
                            }
                        }
                    }
                });
            }
        }
    }
}
