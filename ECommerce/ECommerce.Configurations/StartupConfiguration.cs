﻿using ECommerce.Core.Interfaces;
using ECommerce.Core.Repositories;
using ECommerce.Domain.DTO;
using ECommerce.Domain.Entities;
using ECommerce.Services.Interfaces;
using ECommerce.Services.Services;
using ECommerce.Utility;
using ECommerce.Utility.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using SimpleCrypto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Configurations
{
    public static class StartupConfiguration
    {
        public static IServiceCollection InjectServices(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IECommerceCrypto, ECommerceCrypto>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();

            return services;
        }

        public static IServiceCollection ConfigureJWT(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(ops =>
            {
                ops.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                ops.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(conf =>
            {
                conf.RequireHttpsMetadata = false;
                conf.SaveToken = true;
                conf.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidAudience = configuration[Constants.ConfigurationKeys.SECURITY_INFO_AUDINCE],
                    ValidIssuer = configuration[Constants.ConfigurationKeys.SECURITY_INFO_ISSUER],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration[Constants.ConfigurationKeys.SECURITY_INFO_KEY])),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ClockSkew = TimeSpan.Zero
                };

                conf.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];

                        if (!string.IsNullOrEmpty(accessToken))
                        {
                            context.Token = accessToken;
                        }

                        return Task.CompletedTask;
                    }
                };
            }
            );

            return services;
        }

        public static IServiceCollection ConfigureSwagger(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Inveon Ecommerce Demo App",
                    Version = "v1",
                    Description = "Swagger integration for Demo Ecommerce App",
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                    {
                        Email = "mmgungor94@gmail.com",
                        Name = "Muhammed Gungor",
                        Url = new Uri("https://tr.linkedin.com/in/muhammed-g%C3%BCng%C3%B6r-4b166aa9")
                    }
                });
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                        },
                        new string[] { }
                    }
                    });

                //options.OperationFilter<ImageOperation>();
            });

            return services;
        }

        public static IServiceCollection ConfigureAutoMapper(this IServiceCollection services, IConfiguration configuration, Type type)
        {
            services.AddAutoMapper(config =>
            {
                config.AllowNullDestinationValues = true;

                config.CreateMap<Product, ProductItemResponse>().ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                                                        .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount))
                                                        .ForMember(dest => dest.Piece, opt => opt.MapFrom(src => src.Piece))
                                                        .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Name))
                                                        .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.ProductImages.Select(c => c.Path).ToList()))
                                                        .IgnoreAllPropertiesWithAnInaccessibleSetter()
                                                        .ReverseMap();

                config.CreateMap<Product, ProductItem>().ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                                                        .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount))
                                                        .ForMember(dest => dest.Piece, opt => opt.MapFrom(src => src.Piece))
                                                        .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Name))
                                                        .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                                                        .ForMember(dest => dest.Barcode, opt => opt.MapFrom(src => src.Barcode))
                                                        .IgnoreAllPropertiesWithAnInaccessibleSetter()
                                                        .ReverseMap();
            }, type);

            return services;
        }
    }
}
