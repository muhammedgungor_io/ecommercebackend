﻿using ECommerce.Domain.DTO;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Core.Interfaces
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        /// <summary>
        /// This interface provide to get products with matched own names.
        /// </summary>
        /// <param name="productName"></param>
        /// <returns></returns>
        Task<List<ProductItemResponse>> GetProductsByName(string productName);
    }
}
