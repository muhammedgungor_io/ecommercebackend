﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> SaveChangesAsync();
        void Rollback();


        #region Repository Implementations
        IBaseRepository<T> GetRepository<T>() where T : class;
        IProductRepository ProductRepository { get; }

        #endregion
    }
}
