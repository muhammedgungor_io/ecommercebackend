﻿using ECommerce.Domain.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Core.Middlewares
{
    public class EcommerceException
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public EcommerceException(RequestDelegate next, ILoggerFactory logFactory)
        {
            _next = next;
            _logger = logFactory.CreateLogger("InveonEcommerceExceptionMiddleware");
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    _logger.LogError($"{DateTime.Now} - An Exception Occured : {ex.InnerException.Message}");
                }
                else
                {
                    _logger.LogError($"{DateTime.Now} - An Exception Occured : {ex.Message}");
                }

                await GenerateCommonException(context, ex);
            }
        }

        private async Task GenerateCommonException(HttpContext context, Exception exception)
        {
            var errorMessage = "error";
            var statusCode = HttpStatusCode.BadRequest;
            var exceptionType = exception.GetType();
            switch (exception)
            {
                case Exception e when exceptionType == typeof(UnauthorizedAccessException):
                    statusCode = HttpStatusCode.Unauthorized;
                    errorMessage = "Bu işlemi yapmaya yetkiniz yoktur.";
                    break;

                case CustomApiException e when exceptionType == typeof(CustomApiException):
                    statusCode = (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), e.Code);
                    errorMessage = e.Message;
                    break;

                default:
                    statusCode = HttpStatusCode.InternalServerError;
                    errorMessage = "Bu işlem şu anda gerçekleştirilemiyor.";
                    break;
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;
            context.Items.Add("exception", exception);
            context.Items.Add("exceptionMessage", errorMessage);
        }
    }
    public static class EcommerceExceptionExtensions
    {
        public static IApplicationBuilder UseEcommerceExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<EcommerceException>();
        }
    }
}
