﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ECommerce.Core.Migrations
{
    public partial class _05_SaltCodeAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SaltCode",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SaltCode",
                table: "Users");
        }
    }
}
