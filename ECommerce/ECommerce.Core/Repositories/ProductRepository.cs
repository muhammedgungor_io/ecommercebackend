﻿using ECommerce.Core.Context;
using ECommerce.Core.Interfaces;
using ECommerce.Domain.DTO;
using ECommerce.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Core.Repositories
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        private ECommerceDbContext dbContext;
        public ProductRepository(ECommerceDbContext context) : base(context)
        {
            dbContext = context;
        }

        public async Task<List<ProductItemResponse>> GetProductsByName(string productName)
        {
            var products = await GetMultiple(c => c.Name.Equals(productName));



            return null;
        }
    }
}
