﻿using ECommerce.Core.Context;
using ECommerce.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Core.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(ECommerceDbContext databaseContext) => _dbContext = databaseContext;
        private readonly ECommerceDbContext _dbContext;
        private ProductRepository productRepository;

        private bool disposedValue = false;

        public IProductRepository ProductRepository => productRepository = productRepository ?? new ProductRepository(_dbContext);


        #region Core Database Functions

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IBaseRepository<T> GetRepository<T>() where T : class
        {
            return new BaseRepository<T>(_dbContext);
        }

        public void Rollback()
        {
            _dbContext.ChangeTracker
                    .Entries()
                    .ToList()
                    .ForEach(x => x.Reload());
        }

        public async Task<int> SaveChangesAsync()
        {
            try
            {
                return await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Save Changes Ex.", ex.InnerException);
            }
        }
        #endregion

    }
}
