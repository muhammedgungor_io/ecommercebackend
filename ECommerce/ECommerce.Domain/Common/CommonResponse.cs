﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ECommerce.Domain.Common
{
    public class CommonResponse
    {
        public static CommonResponse Create(HttpStatusCode statusCode, object result = null, string errorMessage = null, string errorCode = null)
        {
            return new CommonResponse(statusCode, result, errorMessage, errorCode);
        }
        public string Version => "1.0";
        public string ErrorMessage { get; set; }
        public object Result { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        protected CommonResponse(HttpStatusCode statusCode, object result = null, string errorMessage = null, string errorCode = null)
        {
            Result = result;
            ErrorMessage = errorMessage;
            StatusCode = statusCode;
        }
    }
}
