﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Domain.Common
{
    /// <summary>
    /// FilterItem class is used in filtering operations.
    /// </summary>
    public class FilterItem
    {
        public string Name { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
}
