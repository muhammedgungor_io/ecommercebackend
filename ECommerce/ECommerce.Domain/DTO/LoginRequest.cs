﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Domain.DTO
{
    public class LoginRequest
    {
        /// <summary>
        /// User login email
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// User login password
        /// </summary>
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
