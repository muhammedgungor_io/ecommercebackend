﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Domain.DTO
{
    public class LoginResponse
    {
        /// <summary>
        /// Authenticated user token
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
