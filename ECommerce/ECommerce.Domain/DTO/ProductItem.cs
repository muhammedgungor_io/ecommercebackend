﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ECommerce.Domain.DTO
{
    public class ProductItem
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("productName")]
        [Required]
        public string ProductName { get; set; }

        [JsonProperty("piece")]
        [Required]
        public int Piece { get; set; }

        [JsonProperty("amount")]
        [Required]
        public string Amount { get; set; }

        [JsonProperty("barcode")]
        [Required]
        public string Barcode { get; set; }

        [JsonProperty("description")]
        [Required]
        public string Description { get; set; }
    }
}
