﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Domain.DTO
{
    public class ProductItemResponse : ProductItem
    {
        [JsonProperty("images")]
        public List<string> Images { get; set; }
    }
}
