﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ECommerce.Domain.Entities
{
    public class Product : BaseEntity
    {
        [StringLength(256)]
        public string Name { get; set; }
        public string Barcode { get; set; }
        public string Amount { get; set; }
        
        [StringLength(1000)]
        public string Description { get; set; }
        public int Piece { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<ProductImage> ProductImages { get; set; }
    }
}
