﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Domain.Entities
{
    public class User : BaseEntity
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string SaltCode { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
