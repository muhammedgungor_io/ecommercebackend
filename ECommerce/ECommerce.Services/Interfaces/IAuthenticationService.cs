﻿using ECommerce.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Services.Interfaces
{
    /// <summary>
    /// User Auth Processes
    /// </summary>
    public interface IAuthenticationService
    {
        /// <summary>
        /// Login service with channel info.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>
        /// <class>LoginResponse</class>
        /// </returns>
        Task<object> LoginAsync(LoginRequest request);
    }
}
