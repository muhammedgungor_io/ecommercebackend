﻿using ECommerce.Domain.Common;
using ECommerce.Domain.DTO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Services.Interfaces
{
    public interface IProductService 
    {
        /// <summary>
        /// Uses for product detail operations.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        Task<ProductItemResponse> GetDetail(Guid productId);

        /// <summary>
        /// Get all T type entity records.
        /// </summary>
        /// <returns></returns>
        Task<List<ProductItemResponse>> GetAll();


        /// <summary>
        /// Get all filtered items which of T type entity records.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<ProductItemResponse>> GetAllByFilter(FilterItem filter);

        /// <summary>
        /// Add a new product
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<bool> AddProduct(ProductItem product,List<IFormFile> images);

        /// <summary>
        /// Update a product
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<bool> UpdateProduct(ProductItem product, List<IFormFile> images);

        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<bool> RemoveProduct(Guid productId);
    }
}
