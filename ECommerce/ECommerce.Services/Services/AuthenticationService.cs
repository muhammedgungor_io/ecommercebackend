﻿using ECommerce.Core.Interfaces;
using ECommerce.Domain.Common;
using ECommerce.Domain.DTO;
using ECommerce.Domain.Entities;
using ECommerce.Services.Interfaces;
using ECommerce.Utility;
using ECommerce.Utility.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SimpleCrypto;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Services.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUnitOfWork _uow;
        private readonly IConfiguration _configuration;
        private readonly IECommerceCrypto _eCommerceCrypto;

        public AuthenticationService(IUnitOfWork uow, IConfiguration configuration, IECommerceCrypto eCommerceCrypto)
        {
            _uow = uow;
            _configuration = configuration;
            _eCommerceCrypto = eCommerceCrypto;
        }


        public async Task<object> LoginAsync(LoginRequest request)
        {
            var user = await _uow.GetRepository<User>().Get(c => !c.IsDeleted && c.Email.Equals(request.Email), includeProperties:"UserRoles");

            if (user == null)
                throw new CustomApiException("404", Constants.GeneralStrings.USER_CREDENTIALS_WRONG);

            if (!_eCommerceCrypto.ValidatePassword(user.Password, user.SaltCode, request.Password))
                throw new CustomApiException("404", Constants.GeneralStrings.USER_CREDENTIALS_WRONG);

            var userRoles = await _uow.GetRepository<UserRole>().GetMultiple(c => c.UserId == user.Id);

            if (userRoles == null)
                throw new CustomApiException("404", Constants.GeneralStrings.USER_CREDENTIALS_WRONG);

            var roles = await _uow.GetRepository<Role>().GetAllAsync();

            var preparedRoles = roles.Where(c => userRoles.Any(x => x.RoleId == c.Id)).Select(c=>c.RoleName).ToList();

            var token = GetToken(user.Email, user.Id.ToString(), preparedRoles);

            var loginResponse = new LoginResponse()
            {
                Name = user.Name,
                Token = new JwtSecurityTokenHandler().WriteToken(token)
            };

            return loginResponse;
        }
       
        private JwtSecurityToken GetToken(string userEmail, string userId,List<string> roles)
        {
            string strRoles = string.Join(",", roles);

            var claims = new[]
            {
                new Claim("sub",userEmail),
                new Claim("jti",Guid.NewGuid().ToString()),
                new Claim("userId",userId),
                new Claim("roles",strRoles)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration[Constants.ConfigurationKeys.SECURITY_INFO_KEY]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var timeout = Convert.ToInt32(_configuration[Constants.ConfigurationKeys.TIMEOUT]);

            var generatedToken = new JwtSecurityToken(
                issuer: _configuration[Constants.ConfigurationKeys.SECURITY_INFO_ISSUER],
                audience: _configuration[Constants.ConfigurationKeys.SECURITY_INFO_AUDINCE],
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(timeout),
                signingCredentials: credentials
            );

            return generatedToken;
        }
    }
}
