﻿using AutoMapper;
using ECommerce.Core.Interfaces;
using ECommerce.Domain.Common;
using ECommerce.Domain.DTO;
using ECommerce.Domain.Entities;
using ECommerce.Services.Interfaces;
using ECommerce.Utility;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Services.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly string folderName = "files";

        public ProductService(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public async Task<ProductItemResponse> GetDetail(Guid productId)
        {
            if (Guid.Empty.Equals(productId))
                throw new CustomApiException("404", Constants.GeneralStrings.PARAMETER_NOT_FOUND);

            var product = await GetProduct(productId);

            var productDetail = _mapper.Map<Product, ProductItemResponse>(product);

            return productDetail;
        }

        public async Task<List<ProductItemResponse>> GetAll()
        {
            var products = await _uow.ProductRepository.GetMultiple(c => !c.IsDeleted, c => c.OrderBy(x => x.AddDate),includeProperties:"ProductImages");

            var response = _mapper.Map<List<Product>, List<ProductItemResponse>>(products.ToList());

            return response;
        }

        public Task<List<ProductItemResponse>> GetAllByFilter(FilterItem filter)
        {
            throw new NotImplementedException();
        }

        private string GetFolderPath()
        {
            return Path.Combine(Directory.GetCurrentDirectory(), folderName);
        }

        private async Task UploadImages(Guid productId, List<IFormFile> images)
        {
            try
            {
                foreach (var image in images)
                {
                    var imageExtension = Path.GetExtension(image.FileName);
                    var imageName = Guid.NewGuid().ToString().Replace("-", "") + imageExtension;
                    var path = Path.Combine(Directory.GetCurrentDirectory(), $"Resources/{imageName}");

                    await using var stream = new FileStream(path, FileMode.Create);

                    await image.CopyToAsync(stream);

                    var productImage = new ProductImage
                    {
                        FileName = imageName,
                        Path = path,
                        ProductId = productId
                    };

                    await _uow.GetRepository<ProductImage>().AddAsync(productImage);
                }
            }
            catch (Exception ex)
            {
                throw new CustomApiException("404",Constants.GeneralStrings.IMAGE_CANNOT_UPLOADED,ex);
            }
        }

        public async Task<bool> AddProduct(ProductItem productItem, List<IFormFile> images)
        {
            var product = _mapper.Map<ProductItem, Product>(productItem);

            await _uow.ProductRepository.AddAsync(product);

            await UploadImages(product.Id, images);

            var response = await _uow.SaveChangesAsync();

            return response > 0;
        }

        public async Task<bool> UpdateProduct(ProductItem productItem, List<IFormFile> images)
        {
            var product = await GetProduct(productItem.Id);

            product.Amount = productItem.Amount;
            product.Barcode = productItem.Barcode;
            product.Description = productItem.Description;
            product.Name = productItem.ProductName;
            product.Piece = productItem.Piece;

            _uow.ProductRepository.Update(product);

            await UploadImages(productItem.Id,images);

            var response = await _uow.SaveChangesAsync();

            return response > 0;
        }

        private async Task FindAndRemovePreviousImages(Guid productId)
        {
            var productImages = await _uow.GetRepository<ProductImage>().GetMultiple(c => c.ProductId == productId);

            if (productImages.Count() > 0)
            {
                foreach (var image in productImages)
                {
                    string imageName = image.FileName;
                    string imagePath = Path.Combine(Directory.GetCurrentDirectory(), $"Resources/{imageName}");
                    
                    FileInfo file = new FileInfo(imagePath);
                    if (file.Exists)
                    {
                        file.Delete();

                        _uow.GetRepository<ProductImage>().Remove(image,true);
                    }
                }
            }
        }

        public async Task<bool> RemoveProduct(Guid productId)
        {
            var product = await GetProduct(productId);

            _uow.ProductRepository.Remove(product);

            await FindAndRemovePreviousImages(productId);

            var response = await _uow.SaveChangesAsync();

            return response > 0;
        }

        private async Task<Product> GetProduct(Guid productId)
        {
            var product = await _uow.ProductRepository.Get(c => c.Id == productId && !c.IsDeleted);

            if (product == null)
                throw new CustomApiException("404", Constants.GeneralStrings.RECORD_NOT_FOUND);

            return product;
        }
    }
}
