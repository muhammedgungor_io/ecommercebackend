﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ECommerce.Utility
{
    public static class Constants
    {
        public static class ConfigurationKeys
        {
            public const string SECURITY_INFO_KEY = "SecurityInfo:Key";
            public const string TIMEOUT = "Timeout";
            public const string SECURITY_INFO_ISSUER = "SecurityInfo:Issuer";
            public const string SECURITY_INFO_AUDINCE = "SecurityInfo:Audince";           
        }

        public static class GeneralStrings
        {
            public const string PARAMETER_NOT_FOUND = "Eksik parametre girilmiştir.";
            public const string RECORD_NOT_FOUND = "Kayıt bulunamadı.";
            public const string IMAGE_CANNOT_UPLOADED = "Ürün resimleri yüklenemedi.";
            public const string USER_CREDENTIALS_WRONG = "Kullanıcı bilgileri doğru girilmedi.";
        }

        public static class ApplicationStandarts
        {
            public const int SALT_SIZE = 25;
            public const int HASH_ITERATIONS = 50;
        }

        public static class Roles
        {
            public const string Admin = "Admin";
        }
    }
}
