﻿using ECommerce.Utility.Interfaces;
using SimpleCrypto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Utility
{
    public class ECommerceCrypto : IECommerceCrypto
    {
        private readonly ICryptoService _cryptoService = new PBKDF2();

        public (string encryptedPassword, string saltCode) SetNewPassword(string userPasswordFromRequest)
        {
            var encrypted = _cryptoService.Compute(userPasswordFromRequest, Constants.ApplicationStandarts.SALT_SIZE, Constants.ApplicationStandarts.HASH_ITERATIONS);
            var saltCode = _cryptoService.Salt;

            return (encrypted, saltCode);
        }

        public bool ValidatePassword(string encryptedPassword, string saltCode, string passwordFromRequest)
        {
            string encrypted = _cryptoService.Compute(passwordFromRequest, salt: saltCode);

            return encrypted == encryptedPassword;
        }

    }
}
