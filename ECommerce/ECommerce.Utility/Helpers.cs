﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Utility
{
    /// <summary>
    /// Helpers class that contains some specific functions to use
    /// </summary>
    public static class Helpers
    {
        public static bool ValidateJson(this string s)
        {
            bool response;
            try
            {
                JToken.Parse(s);
                response = true;
            }
            catch (JsonReaderException ex)
            {
                response = false;
            }

            return response;
        }
    }
}
