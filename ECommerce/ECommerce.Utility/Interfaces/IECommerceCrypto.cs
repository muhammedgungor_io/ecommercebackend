﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECommerce.Utility.Interfaces
{
    public interface IECommerceCrypto
    {
        (string encryptedPassword, string saltCode) SetNewPassword(string userPasswordFromRequest);

        bool ValidatePassword(string encryptedPassword, string saltCode, string passwordFromRequest);
    }
}
