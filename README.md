
# ECommerce Demo App

  

## This project prepared for Inveon

  

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

  
  

##### This project is an application that enables the management of products on an e-commerce site. It actually offers an API service.

  
  

##### The application was carried out by considering it as if it had a front side. This project has API service endpoints that allow us to make changes on it.

  

## Features

- Login

- List of all products

- A product detail

- Adding products

- Product Update

- Deleting a Product

  

##### Since there are authorization processes, the **Normal** user can only see the list of all products and view the details of a product. However, a user with **Admin** authority can make changes on the product.

  
  
  

## Tech

  

- .Net Core

- SQL Server

- LINQ

- Code First

- Swagger

- Dependency Injection

- UnitOfWork

- Repository Pattern

- N-Layer Architecture

- PBKDF2

- Json Web Token (JWT)

- AutoMapper

- Api Response Middleware

- Exception Middleware